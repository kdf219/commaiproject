import cv2


def get_moment(moment):
    if moment['m00'] > 0:
        cx = moment['m10'] / moment['m00']
        cy = moment['m01'] / moment['m00']
        return cx, cy
    return None


training_file = open("train.txt", "r")  # read training file

cap = cv2.VideoCapture("test.mp4")

first = True
use_cnt = []
use_cnt_cvx_hull = []

prev_speed = -1.0

y1, y2 = -1, -1

while cap.isOpened():
    # cv2.waitKey(100)
    _, frame = cap.read()
    frame_full = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
    h, w = frame_full.shape

    frame = frame_full[int(h / 2):int(h / 1.5)]

    kernel = (3, 3)
    frame = cv2.blur(frame, kernel)

    frame = cv2.adaptiveThreshold(frame, 600, cv2.ADAPTIVE_THRESH_GAUSSIAN_C, cv2.THRESH_BINARY, 3, 2)
    frame = cv2.bitwise_not(frame)
    # (frame, 10, 1, 1, 1, 5.0)

    # frame = cv2.morphologyEx(frame, cv2.MORPH_OPEN, kernel)  # close

    _, cnts, _ = cv2.findContours(frame, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_NONE)

    frame = cv2.cvtColor(frame, cv2.COLOR_GRAY2BGR)

    # for cnt in cnts:
    #     cv2.drawContours(frame, [cnt], 0, (50, 10, 100), thickness=2)

    use_cx, use_cy = 0.0, 0.0
    max_cnt_size = -1.0
    for cnt in cnts:
        if first:
            area = cv2.contourArea(cnt)

            if area > max_cnt_size:
                use_cnt = cnt
                use_cnt_cvx_hull = cv2.convexHull(cnt)
                moment = get_moment(cv2.moments(cnt))
                if moment is not None:
                    use_cx, use_cy = moment[0], moment[1]

            continue

        # if True:
        cvx_hull = cv2.convexHull(cnt)
        arc = cv2.arcLength(cnt, True)
        diff = cv2.arcLength(use_cnt, True) - arc
        if cv2.contourArea(use_cnt) == cv2.contourArea(cnt):
            # if [cvx_hull.all()] == [use_cnt_cvx_hull.all()] and cv2.arcLength(use_cnt, True) == arc:
            # this is the same cnt
            use_cnt = cnt

            moment = cv2.moments(cnt)
            true_speed = -1.0

            try:
                true_speed = float(training_file.readline())
            except ValueError:
                ()

            moment = get_moment(moment)
            if moment is not None:
                cx, cy = moment[0], moment[1]
                # print(cx, cy)

                diff_cx, diff_cy = (use_cx - cx), (use_cy - cy)
                # todo need to choose a new cnt when the one starts to go off the screen
                if abs(diff_cy) < 5:  # if y is < 5 I think same cnt
                    # print('diffs', diff_cx, diff_cy)

                    y2 = y1
                    y1 = cy

                    # print("ys", (y2 - y1), prev_speed, true_speed)
                    print((y2 - y1), (true_speed - prev_speed))
                    prev_speed = true_speed

                    cv2.drawContours(frame, [cnt], 0, (50, 10, 100), thickness=10)

                    cv2.imshow("frame", frame)

                    while True:
                        k = cv2.waitKey(1)
                        if k == ord('x'):
                            break
                    break
                # else:
                #     print('not found')

                # cv2.drawContours(frame, [cnts[len(cnts) - 1]], 0, (50, 10, 100), thickness=2)
                # cv2.drawContours(frame, [cnts[len(cnts) - 2]], 0, (50, 10, 10), thickness=2)

    first = False
    cv2.imshow("1frame", frame)
    cv2.waitKey(1)
