import cv2
import numpy as np
from sklearn import metrics

# write_file = open("write.txt", "w")
# answer_file = open("answers.txt", "w")
test_file = open("test.txt", "w")


def wait():
    while True:
        k = cv2.waitKey(1)
        if k == ord('x'):
            return
        if k == ord('z'):
            # write_file.close()
            exit(0)


class Contour:
    def __init__(self, cnt):
        # self.location = (get_corner(cnt))  # x, y coordinate of top left corner
        #
        # self.prev_location = (-1.0, -1.0)
        #
        # self.loc_diff = 0.0
        self.cnt = cnt
        self.x, self.y = get_corner(cnt)
        self.prev_x, self.prev_y = None, None

        self.y = abs(self.y)

    def move(self, idx, cnts):
        if idx >= len(cnts):
            return self.prev_x - self.x, self.prev_y - self.y

        cnt = cnts[idx]
        # if self.prev_y is not None:
        # print('moved', self.dist_moved())
        # print('moved', self.prev_y, self.y)

        self.prev_x = self.x
        self.prev_y = self.y

        max_dist = 10

        if self.y < self.prev_y:
            # different contour -- need to check fell of screen case
            for cnt_candidate in cnts:
                if get_loc_diff(self.cnt, cnt_candidate) < max_dist:
                    self.cnt = cnt_candidate
        else:
            self.cnt = cnt

        self.cnt = cnt

        self.x, self.y = get_corner(self.cnt)
        self.y = abs(self.y)

        return self.prev_x - self.x, self.prev_y - self.y  # returns x, y distance moved

    def dist_moved(self):
        return np.sqrt(np.square(abs(self.prev_x) - abs(self.x)) + np.square(abs(self.prev_y) - abs(self.y)))

    def get_corner(self):
        box = cv2.boxPoints(cv2.minAreaRect(self.cnt))
        pt = box[2]
        return pt[0], pt[1]


# def move(self, moved_cnt):
#     self.prev_location = self.location
#     self.location = get_corner(moved_cnt)
#     self.loc_diff = get_loc_diff(self.cnt, moved_cnt)


def get_loc_diff(cnt1, cnt2):  # takes in Contours
    cnt1_x, cnt1_y = get_corner(cnt1)
    cnt2_x, cnt2_y = get_corner(cnt2)
    return np.sqrt(np.square(cnt1_y - cnt2_y) + np.square(cnt1_x - cnt2_x))


def get_corner(cnt):
    box = cv2.boxPoints(cv2.minAreaRect(cnt))
    pt = box[2]
    return pt[0], pt[1]


use_m = 0.000643322880528432
use_b = -0.018536050262139576

avg = 0
avg_counter = 0

training_file = open("train.txt", "r")  # read training file

cap = cv2.VideoCapture("test.mp4")

prev_frame = None
prev_x, prev_y, prev_speed, true_speed = -1.0, -1.0, -1.0, -1.0
locs, speed = [], []
prev_r = 0.0

primary_cnt, secondary_cnt = None, None

speed_changes, y_vals = [], []

diff_vals = []
true_speeds, pred_speeds = [], []

while cap.isOpened():
    cv2.waitKey(1)
    _, frame = cap.read()
    frame = cv2.resize(frame, (0, 0), fx=3, fy=3)
    frame = cv2.cvtColor(frame, cv2.COLOR_BGR2HSV)
    # cv2.imshow("frame", frame)
    h, w, _ = frame.shape
    show = frame[int(h / 2.2):int(h / 1.5), int(w / 3):int(w / 2)]
    h, w, _ = show.shape

    change = cv2.mean(cv2.cvtColor(cv2.cvtColor(show.copy(), cv2.COLOR_HSV2RGB), cv2.COLOR_RGB2GRAY))[
        0]  # higher = more white
    # print('change', change, cv2.mean(frame.copy())[0])
    # cv2.imshow('chna', show)

    # want change ~40

    goal_change = 40

    low_change = 100 - (goal_change - change)
    high_change = 360 - (goal_change - change)

    low, high = (0, 0, low_change), (300, 300, high_change)

    show = cv2.inRange(show, low, high)
    show = cv2.morphologyEx(show, cv2.MORPH_CLOSE, (5, 5))
    show = cv2.morphologyEx(show, cv2.MORPH_CLOSE, (5, 5))

    _, cnts, _ = cv2.findContours(show, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_NONE)

    if len(cnts) > 0:
        prev_cnt = Contour(cnts[0])

    if primary_cnt is None:
        primary_cnt = Contour(cnts[0])
        secondary_cnt = Contour(cnts[1])
    else:
        # y should be larger than prev_y or it it fell off the screen
        # if primary_cnt.y < primary_cnt.prev_y:
        # fell off screen
        # todo fix this
        # primary_cnt = secondary_cnt
        # print('fell off', primary_cnt.y)

        cv2.drawContours(show, [primary_cnt.cnt.copy()], 0, (200, 10, 200), thickness=2)  # draw prev

        primary_cnt.move(0, cnts)
        secondary_cnt.move(1, cnts)

        # write_file.write(str(primary_cnt.dist_moved()))

    # need to switch cnts when y value decreases -- falls off screen

    show = cv2.cvtColor(show, cv2.COLOR_GRAY2BGR)
    # for cnt in cnts:
    #     cv2.drawContours(show, [cnt], 0, (50, 10, 100), thickness=2)
    # prim_moment = None
    # x, y = 0.0, 0.0
    prev_speed = true_speed
    true_speed = float(training_file.readline())

    # prim_cnt = cnts[0]
    # sec_cnt = cnts[1]
    # cv2.drawContours(show, [prim_cnt], 0, (50, 10, 100), thickness=2)
    cv2.drawContours(show, [primary_cnt.cnt], 0, (50, 10, 100), thickness=2)

    # cv2.drawContours(show, [sec_cnt], 0, (0, 0, 100), thickness=2)
    cv2.drawContours(show, [secondary_cnt.cnt], 0, (200, 200, 10), thickness=2)
    # x, y = primary_cnt.get_corner()
    cv2.circle(show, (primary_cnt.x, primary_cnt.y), 2, (180, 100, 200), thickness=4)
    # prim_moment = get_moment(cnt=prim_cnt)  # cv2.moments(cnts[0]))
    # if prim_moment is not None:
    #     x, y = prim_moment[0], prim_moment[1]
    #     # print(prev_x - x, prev_y - y, true_speed - prev_speed)
    #     if prev_y > 0:
    #         loc_diff = np.sqrt(np.square(y - prev_y) + np.square(x - prev_x))
    #         locs.append(loc_diff)
    #         print(loc_diff, (true_speed - prev_speed), (true_speed - prev_speed) / loc_diff)
    #         cv2.circle(show, (x, y), 2, (100, 100, 10), thickness=4)
    #     prev_x, prev_y = x, y
    #     if prev_speed > 0:
    #         speed.append(true_speed - prev_speed)
    #
    #     speed_guess = use_m * y + use_b
    #     # print(true_speed - prev_speed, speed_guess - (true_speed - prev_speed))
    #     avg += true_speed * (true_speed - prev_speed)
    #     avg_counter += 1
    #     # print(avg / avg_counter)

    # prev_speed = true_speed

    if primary_cnt.prev_y is not None:
        # print((true_speed - prev_speed) / (primary_cnt.y - primary_cnt.prev_y))
        velocity = (primary_cnt.y - primary_cnt.prev_y) * (20 * 60 * 60)  # v in some dist/hr
        # print(velocity, true_speed, true_speed / (primary_cnt.y * velocity))
        # print(velocity, primary_cnt.y, true_speed)

        # write_file.write('\n' + str(velocity))
        # write_file.write('\n')

    cv2.imshow("show", show)

    a, b, c, d = .0000876812, 0.0000118337, .0000898962, 27.9319 - 9

    if primary_cnt.prev_y is not None:
        t_speed = a * np.square(primary_cnt.dist_moved()) + b * np.square(
            primary_cnt.y) + c * primary_cnt.dist_moved() * primary_cnt.y + d

        if primary_cnt.dist_moved() < 0.01:
            t_speed = 0.0

        print('\n', true_speed, t_speed, true_speed - t_speed)

        diff_vals.append(true_speed - t_speed)

        true_speeds.append(true_speed)
        pred_speeds.append(t_speed)

        mean = np.mean(diff_vals)
        print('mean', mean, len(diff_vals), mean / len(diff_vals))
        test_file.write(str(t_speed) + '\n')

        # while True:
    #     k = cv2.waitKey(1)
    #     if k == ord('y'):
    #         if primary_cnt.prev_y is not None:
    #             speed_changes.append(true_speed - prev_speed)
    #             y_vals.append(primary_cnt.y)
    #         break
    #     elif k == ord('x'):
    #         break
    #
    # if len(y_vals) > 2:
    #     gradient, intercept, r_value, p_value, std_err = stats.linregress(y_vals, speed_changes)
    #     mn = np.min(y_vals)
    #     mx = np.max(y_vals)
    #     x1 = np.linspace(mn, mx, 500)
    #     y1 = gradient * x1 + intercept
    #     plt.plot(y_vals, speed_changes, 'ob')
    #     plt.plot(x1, y1, '-r')
    #     plt.show()
    if len(true_speeds) > 0:
        print("mean sq error", metrics.mean_squared_error(true_speeds, pred_speeds))
    # wait()
    cv2.waitKey(10)

    # if len(speed) > 0:
    #     # lingress_results = linregress(locs, speed)
    #     # m, b = lingress_results[0], lingress_results[1]
    #     #
    #     # print(m, b, lingress_results[2])
    #     # speed = m * loc + b
    #
    #     # gradient, intercept, r_value, p_value, std_err = stats.linregress(locs, speed)
    #     # print(r_value * prev_r)
    #     # mn = np.min(locs)
    #     # mx = np.max(locs)
    #     # x1 = np.linspace(mn, mx, 500)
    #     # y1 = gradient * x1 + intercept
    #     # plt.plot(locs, speed, 'ob')
    #     # plt.plot(x1, y1, '-r')
    #     # plt.show()
    #     #
    #     # prev_r = r_value
    #
    #     print(locs[len(locs) - 1])

    # while True:
    #     k = cv2.waitKey(1)
    #     if k == ord('x'):
    #         break

test_file.close()
