import cv2, numpy as np

training_file = open("train.txt", "r")  # read training file

cap = cv2.VideoCapture("train.mp4")

prev_frame = None

while cap.isOpened():
    cv2.waitKey(1)
    _, frame = cap.read()
    frame = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
    h, w = frame.shape

    frame = frame[int(h / 2):int(h / 1.5)]

    kernel = (7, 7)
    # frame = cv2.blur(frame, kernel)
    #
    # frame = cv2.adaptiveThreshold(frame, 600, cv2.ADAPTIVE_THRESH_GAUSSIAN_C, cv2.THRESH_BINARY, 3, 2)
    # frame = cv2.bitwise_not(frame)

    if prev_frame is not None:
        frame_diff = cv2.absdiff(frame, prev_frame)
        frame_diff = cv2.morphologyEx(frame_diff, cv2.MORPH_CLOSE, kernel)
        frame_diff = cv2.morphologyEx(frame_diff, cv2.MORPH_OPEN, kernel)
        frame_diff = cv2.morphologyEx(frame_diff, cv2.MORPH_OPEN, kernel)

        n_white_pix = np.sum(frame_diff > 0)

        true_speed = training_file.readline()

        print(n_white_pix, true_speed)
        cv2.imshow('diff', frame_diff)

        while True:
            k = cv2.waitKey(1)
            if k == ord('x'):
                break

    prev_frame = frame
